const template = require('../Schema/Template')

const createTheTemplate = async ( req, res) => {
    try {
        const data = await template.create(req.body)
        res.send("done")
    } catch (error) {
        console.log(error)
    }
}

const getAllTheTemplate = async (req, res)=>{
    try {
        const data = await template.find({})
        res.send(data)
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    createTheTemplate : createTheTemplate,
    getAllTheTemplate : getAllTheTemplate
}