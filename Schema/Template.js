const mongoose = require('mongoose')
const schema = mongoose.Schema

const Template = new schema({
    object : String
})

module.exports = mongoose.model("Template", Template)