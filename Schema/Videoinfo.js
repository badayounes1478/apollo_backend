const mongoose = require('mongoose')
const schema = mongoose.Schema

const Video = new schema({
    user_id: {
        type: String
    },
    timeStamp: {
        type: Date,
        default: new Date()
    },
    url: {
        type: String
    },
    status: {
        type: String
    },
    template: {
        type: String
    },
    path: {
        type: String
    }
})

module.exports = mongoose.model("Video", Video)