const router = require('express').Router()
const { generateTheVideo } = require('../Controller/VideoGenerator')

router.post("/video/:id", generateTheVideo)

module.exports = router