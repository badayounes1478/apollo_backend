const express = require('express')
const router = express.Router()
const {createTheTemplate, getAllTheTemplate} =  require('../Controller/Template')

router.post('/', createTheTemplate)

router.get('/', getAllTheTemplate)

module.exports = router