var ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
var ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);
const express = require('express')
const cors = require('cors')
const app = express()
const mongoose = require('mongoose')

app.use(express.json({ limit: '200mb' }))
app.use(cors())
app.use(express.static('build'))
mongoose.connect('mongodb://localhost:27017/Apollo', { useNewUrlParser: true, useUnifiedTopology: true });

app.use('/apollo/template', require('./Router/Template'))
app.use('/apollo/generate', require('./Router/VideoGenerator'))
app.use('/apollo/video', require('./Router/Video'))

app.listen(process.env.PORT || 4001, () => {
    console.log("listening")
})


